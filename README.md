# n-puzzle

The goal of this project is to solve the N-puzzle ("taquin" in French) game using the A*search algorithm or one of its variants.

Language: `ruby`

## Project

Implement the A* search algorithm (or one of its variants, you’re free to choose) to solvean N-puzzle, with the following constraints:

- You have to manage various puzzle sizes (3, 4, 5, 17, etc ...). The higher your program can go without dying a horrible, horrible death, the better.

- You have to manage both randomly determined states (of your own generation of course), or input files that specify a starting board, the format of which is describedin the appendix.

- The cost associated with each transition is always 1.

- The user must be able to choose between at LEAST 3 (relevant) heuristic functions. The Manhattan-distance heuristic is mandatory, the other two are up to you. By"relevant" we mean they must be admissible (Read up on what this means) and they must be something other than "just return a random value because #YOLO".

- At the end of the search, the program has to provide the following values:

  - Total number of states ever selected in the "opened" set (complexity in time).
  
  - Maximum number of states ever represented in memory at the same time during the search (complexity in size).
  
  - Number of moves required to transition from the initial state to the final state, according to the search.
  
  - The ordered sequence of states that make up the solution, according to thesearch.
  
  - The puzzle may be unsolvable, in which case you have to inform the user and exit.
