class Heuristics
  def initialize(heuristic)
    if heuristic == :heuristic 
      @heuristic = method(:manhattan)
    elsif heuristic == :hamming
      @heuristic = method(:hamming)
    else
      @heuristic = method(:linear_conflict)
    end
  end
  def call puzzle
    @heuristic.call puzzle
  end
  def manhattan(puzzle)
    score = 0
    puzzle.puzzle.each_with_index do |square, index|
      x = index % puzzle.size
      y = index / puzzle.size
      ax = (square - 1) % puzzle.size
      ay = (square - 1) / puzzle.size
      score += (x - ax).abs + (y - ay).abs unless square == 0
    end
    score
  end
  def hamming puzzle
    score = 0
    puzzle.puzzle.each_with_index do |square, index|
      score += 1 if square - (index + 1) != 0 if square != 0
    end
    score
  end
  def linear_conflict puzzle
    score = manhattan puzzle
    inCol = []
    inRow = []
    conflicts = 0
    puzzle.puzzle.each_with_index do |square, index|
      x = index  % puzzle.size 
      y = index / puzzle.size
      ax = (square - 1) % puzzle.size
      ay = (square - 1) / puzzle.size
      inCol[index] = ax == x
      inRow[index] = ay == y
    end
    (0...puzzle.size).each do |y|
      (0...puzzle.size).each do |x|
        i = y * puzzle.size + x
        next if puzzle.puzzle[i] == 0
        if inCol[i]
          (y...puzzle.size).each do |r|
            j = r * puzzle.size + x
            next if puzzle.puzzle[j] == 0
            conflicts += 1 if inCol[j] and puzzle.puzzle[j] < puzzle.puzzle[i]
          end
        end
        if inRow[i]
          (x...puzzle.size).each do |c|
            j = y * puzzle.size + c
            next if puzzle.puzzle[j] == 0
            conflicts += 1 if inRow[j] and puzzle.puzzle[j] < puzzle.puzzle[i]
          end
        end
      end
    end
    score + 2 * conflicts
  end
end
