class Solver
  def initialize algo
    @solver = method(:astar)
  end
  def solve puzzle, heuristic
    @solver.call puzzle, heuristic
  end
  def astar startState, heuristic
    result = {:path => nil, :Otime => 0, :Osize => 0}
    openSet = PriorityQueue.new
    fScore = Hash.new(Float::INFINITY)
    fScore[startState] = heuristic.call(startState)
    closedList = Hash.new(false)
    openSet.insert startState, fScore[startState]
    cameFrom = {}
    gScore = Hash.new(Float::INFINITY)
    gScore[startState] = 0
    while (openSet.length != 0)
      current = openSet.pop
      if current.sorted?
        path = [current]
        while current
          current = cameFrom[current]
          path.push(current)
        end
        result[:path] = path.compact.reverse
        return result
      end
      closedList[current] = true
      current.generate_neighbors.each do |neighbor|
        if gScore[current] + 1 < gScore[neighbor]
          next if closedList[neighbor]
          cameFrom[neighbor] = current
          gScore[neighbor] = gScore[current] + 1
          fScore[neighbor] = gScore[neighbor] + heuristic.call(neighbor)
          if not openSet.include? neighbor
            $Logger.verbose_puts "Added #{neighbor} to openSet (#{openSet.size}) with score #{fScore[neighbor]} (#{gScore[neighbor]} + #{heuristic.call(neighbor)})", 3
            openSet.insert neighbor, fScore[neighbor]
            result[:Otime] += 1
          end
        end
      end
      result[:Osize] = [result[:Osize], openSet.size].max
    end
    return result 
  end
end
