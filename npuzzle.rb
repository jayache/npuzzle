require './src/Logger'
require './src/Parser'
require './src/Puzzle'
require './src/PriorityQueue'
require './src/Heuristics'
require './src/Solver'
require 'optparse'
require 'stackprof'

def pretty_print_line(puzzles, height)
  line = ''
  size = puzzles[0].size
  pad = Math.log(size**2, 10)
  puzzles.each do |puzzle| # each zll
    next if puzzle.nil?

    slice = puzzle.puzzle[(size * height)...(size * height + size)]
    slice.map! { |n| n.to_s.rjust(pad, '0') }
    line += slice.join
    line += if height == size / 2
              ' => '
            else
              '    '
            end
  end
  $Logger.verbose_puts line, 2
end

def pretty_print_puzzle(path)
  path.each_slice(3) do |triple|
    size = triple[0].size
    size.times do |i|
      pretty_print_line(triple, i)
    end
    $Logger.verbose_puts '', 2
  end
end

def npuzzle solver, heuristic, puzzle
  time = Time.now
  result = solver.solve(puzzle, heuristic)
  solved = Time.now
  $Logger.puts "#{result[:path].reverse.join("=>")} (#{result[:path].size})"
  $Logger.puts "Nodes in memory: #{result[:Osize]} and total nodes checked: #{result[:Otime]}"
  pretty_print_puzzle result[:path]
  $Logger.verbose_puts "Solved in #{solved - time}", 1
  solved - time
end

begin
  options = {:puzzle => [], :parameter => [], :heuristic => :manhattan, :verbose => 1, :debug => 0, :autogenerate => 0, :generatesize => 3}
  OptionParser.new do |opt|
    opt.on('-v', '--verbose [LEVEL]', "Program verbosity, defaults to 1", Integer) {|o| options[:verbose] = o || 1}
    opt.on('-p', '--parameter PUZZLE') {|o| options[:puzzle].push(o)} #mouais
    opt.on('-h', '--help', "Show this message") { puts opt; exit }
    opt.on('-H', '--heuristic HEURISTIC', [:manhattan, :hamming, :linear], " Heuristic (manhattan, hamming, linear)") {|o| options[:heuristic] = o}
    opt.on('-d', '--debug [LEVEL]', "Add debug statements, defaults to 1", Integer) {|o| options[:debug] = o || 1}
    opt.on('-g', '--generate NUMBER', "Generate NUMBER npuzzle, and solve them all in succession.", Integer) {|o| options[:autogenerate] = o}
    opt.on('-s', '--size NUMBER', "All generated puzzle will be of size NUMBER", Integer) {|o| options[:generatesize] = o} 
  end.parse!
  options[:file] = ARGV
rescue OptionParser::InvalidArgument => e
  STDERR.puts "Invalid argument provided: #{e.message}"
  exit 1
rescue OptionParser::MissingArgument => e
  STDERR.puts "Missing argument : #{e.message}"
  exit 1
end
start = []
$Logger = Logger.new(options[:verbose], options[:debug], STDOUT) # File.open("Test.log", "w"))
solver = Solver.new "astar"
heuristic = Heuristics.new(options[:heuristic])
total = 0
nbtests = 0
options[:file].each do |filename|
  begin
    file = File.open filename
    start = Parser.parse_file file
  rescue OptionParser::InvalidArgument => e
    STDERR.puts "Skipping #{filename} because: #{e.message}"
  rescue Errno::ENOENT
    STDERR.puts "Skipping #{filename} because we cannot find it"
  else
    $Logger.verbose_puts "Solving #{start.puzzle}...", 1
    total += npuzzle solver, heuristic, start
    nbtests += 1
  end
end
options[:puzzle].each do |argument|
  begin
    start = Parser.parse_commandline argument
  rescue OptionParser::InvalidArgument => e
    STDERR.puts "Skipping #{argument} because: #{e.message}"
  else
    $Logger.verbose_puts "Solving #{start.puzzle}...", 1
    total += npuzzle solver, heuristic, start
    nbtests += 1
  end
end

options[:autogenerate].times do
  start = Puzzle.new(options[:generatesize], (0...(options[:generatesize] * options[:generatesize])).to_a)
  100.times do 
    start = start.generate_neighbors.sample
  end
  $Logger.verbose_puts "Solving #{start.puzzle}...", 1
    total += npuzzle solver, heuristic, start
  nbtests += 1
end
$Logger.verbose_puts "Solved #{nbtests} puzzle in : #{total}. Avg: #{total / nbtests}", 1 if nbtests > 1
$Logger.puts "Nothing to do..." if nbtests == 0

