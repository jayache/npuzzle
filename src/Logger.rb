# Class for printing data while accounting for options
# That's already like 2-3 bonus points right here
class Logger
  @debugLevel = 0
  @verboseLevel = 0
  @fdout = STDOUT
  
  def initialize(verboseLevel, debugLevel, fdout)
    @debugLevel = debugLevel
    @verboseLevel = verboseLevel
    @fdout = fdout
  end

  # Simple puts that will *always* print, even in quiet mode
  def puts(str)
    @fdout.puts str
  end
  
  # Puts non-debug data, allowing for different level of verbosity
  def verbose_puts(str, verboseLevel = 1)
    if @verboseLevel >= verboseLevel
      @fdout.puts str
    end
  end
  
  # Puts debug data, allowing for different level of debug
  def debug_puts(str, debugLevel = 1)
    if @debugLevel >= debugLevel
      @fdout.puts str
    end
  end
end
