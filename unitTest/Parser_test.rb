require_relative '../src/Puzzle.rb'
require_relative '../src/Logger.rb'
require_relative '../src/Parser.rb'
require "test/unit/assertions"
require "test/unit"
include Test::Unit::Assertions

$Logger = Logger.new(1, 1, File.open("ParserTest.log", "w"))

class ParserTest < Test::Unit::TestCase
  def test_commandlineparser
    assert_equal [0, 1, 2, 3, 4, 5, 6, 7, 8], Parser.parse_commandline("0 1 2 3 4 5 6 7 8").puzzle
    assert_equal [0, 1, 2, 3, 4, 7, 6, 5, 8], Parser.parse_commandline("0 1 2 3 4 7 6 5 8").puzzle
    assert_equal [0, 1, 2, 3], Parser.parse_commandline("0 1 2 3").puzzle

    assert_equal [0, 1, 2, 3], Parser.parse_commandline("0123").puzzle
    assert_equal [0, 1, 2, 3, 4, 5, 6, 7, 8], Parser.parse_commandline("012345678").puzzle
    assert_equal [0, 1, 2, 3, 4, 7, 6, 5, 8], Parser.parse_commandline("012347658").puzzle

    assert_raise("OptionParser::InvalidArgument") { Parser.parse_commandline("toto") }
    assert_raise("OptionParser::InvalidArgument"){ Parser.parse_commandline("0 0 0 0 0 0 0") }
    assert_raise("OptionParser::InvalidArgument"){ Parser.parse_commandline("0123456789101112131415")}
    assert_raise("OptionParser::InvalidArgument"){ Parser.parse_commandline("0 1 2 3 4 5 6")}
    assert_raise("OptionParser::InvalidArgument"){ Parser.parse_commandline("")}
    assert_raise("OptionParser::InvalidArgument"){ Parser.parse_commandline("123g45678")}
    assert_raise("OptionParser::InvalidArgument"){ Parser.parse_commandline("012345679")}
  end

  def test_fileparser
    assert_equal [1, 2, 3, 0], Parser.parse_file(File.open("ressources/test_2")).puzzle
    assert_equal [8, 7, 2, 4, 0, 6, 3, 5, 1], Parser.parse_file(File.open("ressources/test_3")).puzzle
    assert_equal [5, 6, 7, 8, 2, 1, 14, 10, 9, 12, 11, 3, 0, 15, 4, 13], Parser.parse_file(File.open("ressources/test_4")).puzzle
  end
end
