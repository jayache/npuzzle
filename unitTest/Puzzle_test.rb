require_relative '../src/PriorityQueue.rb'
require_relative '../src/Puzzle.rb'
require_relative '../src/Logger.rb'
require "test/unit/assertions"
require "test/unit"
include Test::Unit::Assertions

$Logger = Logger.new(1, 1, File.open("Test.log", "w"))

class PuzzleTest < Test::Unit::TestCase
  def test_sorted
    puzzle1 = Puzzle.new(1, [0])
    puzzle2 = Puzzle.new(2, [1, 2, 3, 0])
    puzzle3 = Puzzle.new(3, [1, 2, 3, 4, 5, 6, 7, 8, 0])
    puzzle4 = Puzzle.new(4, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0])
    puzzle5 = Puzzle.new(5, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0])

    assert puzzle1.sorted?
    assert puzzle2.sorted?
    assert puzzle3.sorted?
    assert puzzle4.sorted?
    assert puzzle5.sorted?

    puzzle2 = Puzzle.new(2, [3, 0, 1, 2])
    puzzle3 = Puzzle.new(3, [0, 1, 2, 3, 4, 5, 7, 6, 8])
    puzzle4 = Puzzle.new(4, [1, 2, 4, 3, 5, 6, 7, 8, 9, 10, 0, 11, 12, 13, 14, 15])
    puzzle4b = Puzzle.new(4, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 11, 12, 13, 14, 15])
    puzzle5 = Puzzle.new(5, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 23, 0])

    assert_false puzzle2.sorted?
    assert_false puzzle3.sorted?
    assert_false puzzle4.sorted?
    assert_false puzzle4b.sorted?
    assert_false puzzle5.sorted?
  end
  def test_neighbors
    puzzle2 = Puzzle.new(2, [0, 3, 2, 1])
    puzzle3 = Puzzle.new(3, [1, 2, 0, 3, 4, 6, 7, 5])

    puzzle2n = puzzle2.generate_neighbors
    puzzle3n = puzzle3.generate_neighbors

    assert_equal 2, puzzle2n.size
    assert_equal 3, puzzle3n.size

    assert_equal [3, 0, 2, 1], puzzle2n[0].puzzle
    assert_equal [2, 3, 0, 1], puzzle2n[1].puzzle
  end
end
