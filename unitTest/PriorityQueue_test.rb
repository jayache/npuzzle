require_relative '../src/PriorityQueue.rb'
require_relative '../src/Logger.rb'
require "test/unit/assertions"
require "test/unit"
include Test::Unit::Assertions

$Logger = Logger.new(3, 1, File.open("Test.log", "w"))

class PriorityQueueTest < Test::Unit::TestCase
  def test_insert
    queue = PriorityQueue.new
    15.times do |i|
      queue.insert i, i
    end
    assert_equal 15, queue.size
    queue.insert 0, 0
    assert_equal 16, queue.size
  end
 
  def test_pop
    array = []
    20.times do |i|
      array.push([i, rand(-5000..5000)])
    end
    queue = PriorityQueue.new
    array.each do |elem|
      queue.insert elem[0], elem[1]
    end
    min = array.min {|a, b| a[1] <=> b[1]} 
    assert_equal min[0], queue.pop
    array.delete(min)
    min = array.min {|a, b| a[1] <=> b[1]} 
    assert_equal min[0], queue.pop
    array.delete(min)
    min = array.min {|a, b| a[1] <=> b[1]} 
    assert_equal min[0], queue.pop
    array.delete(min)
    min = array.min {|a, b| a[1] <=> b[1]} 
    assert_equal min[0], queue.pop
    array.delete(min)
  end
  def test_include
    queue = PriorityQueue.new
    (-10..10).each do |elem|
      queue.insert elem, elem
    end
    assert (queue.include? 0)
    assert (queue.include? (-9))
    assert (queue.include? 9)
    assert_false (queue.include? (-50))
  end
end
