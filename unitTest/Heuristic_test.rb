require_relative '../src/Puzzle.rb'
require_relative '../src/Logger.rb'
require_relative '../src/Heuristics.rb'
require "test/unit/assertions"
require "test/unit"
include Test::Unit::Assertions

$Logger = Logger.new(1, 1, File.open("HeuristicTest.log", "w"))

class HeuristicTest < Test::Unit::TestCase
  def test_manhattan
    heuristic = Heuristics.new(:manhattan)
    assert_equal 10, heuristic.call(Puzzle.new(3, [8, 1, 3, 4, 0, 2, 7, 6, 5]))
    assert_equal 4, heuristic.call(Puzzle.new(3, [0, 1, 3, 4, 2, 5, 7, 8, 6]))
    assert_equal 3, heuristic.call(Puzzle.new(3, [1, 0, 3, 4, 2, 5, 7, 8, 6]))
    assert_equal 5, heuristic.call(Puzzle.new(3, [4, 1, 3, 0, 2, 5, 7, 8, 6]))
  end
  def test_hamming
    heuristic = Heuristics.new(:hamming)
    assert_equal 8, heuristic.call(Puzzle.new(3, [5, 4, 6, 0, 2, 7, 8, 1, 3])) 
    assert_equal 7, heuristic.call(Puzzle.new(3, [1, 4, 6, 0, 2, 7, 8, 5, 3])) 
    assert_equal 0, heuristic.call(Puzzle.new(3, [1, 2, 3, 4, 5, 6, 7, 8, 0])) 
  end
  def test_linear
    heuristic = Heuristics.new(:linear_conflict)

    assert_equal 14, heuristic.call(Puzzle.new(3, [4, 2, 5, 1, 0, 6, 3, 8, 7]))
  end
end
