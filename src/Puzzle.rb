# Class representing a state of npuzzle
class Puzzle
  attr_reader :puzzle
  attr_accessor :fscore
  attr_reader :size
  def initialize(size, shape)
    @puzzle = shape
    @size = size.to_i
  end

  def hash
    @puzzle.hash
  end
  # Returns true if the puzzle is sorted (goal state)
  # Currently only skips the empty square. May need to make it stricter later.
  def sorted?
    @puzzle[0...-1].sort == @puzzle[0...-1]
  end

  def <=> puzzle
    @fscore <=> puzzle.fscore
  end

  # Returns true if the two puzzle are equals
  def eql? puzzle
    puzzle.puzzle.zip(@puzzle).each do |a, b|
      if a != b
        return false
      end
    end
    true
  end

  # Returns a string representation of the class
  def to_s
    @puzzle.to_s
  end
  
  # Generates all the possible neighboring states
  def generate_neighbors
    #$Logger.debug_puts "Generate neighbors of #{@puzzle}"
    neighbors = []
    potentialMoves = [-1, -@size, 1, @size]
    zero = @puzzle.index(0)
    potentialMoves.each do |move|
      if zero + move >= 0 and zero + move < @puzzle.size
        if (zero / size != (zero + move) / size and (move == 1 or move == -1))
          next
        end
        neighbor = @puzzle.dup
        neighbor[zero] = @puzzle[zero + move]
        neighbor[zero + move] = 0
        neighbors.push(Puzzle.new(@size, neighbor))
      end
    end
    #$Logger.debug_puts "Generated: #{neighbors}"
    neighbors
  end
end
