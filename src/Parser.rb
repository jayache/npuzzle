require 'optparse'
# Static class whose job is to handle parsing and sanity checking of npuzzles
class Parser
  # Parse command line format
  def self.parse_commandline line
    if line.include?(" ")
      split = line.split
    else
      split = line.split('')
    end
    size = Math.sqrt(split.size)
    sanitycheck size, split
    split.map!(&:to_i)
    Puzzle.new(size, split)
  end
  # Performs sanity check on input, raise error if input is bad
  # Array is still a string
  def self.sanitycheck size, puzzle
    raise OptionParser::InvalidArgument if puzzle.any? {|n| n.to_i.to_s != n} 
    puzzle = puzzle.map(&:to_i)
    raise OptionParser::InvalidArgument if puzzle != puzzle.uniq
    raise OptionParser::InvalidArgument if size.to_i != size or size < 1
    fsize = size * size - 1
    raise OptionParser::InvalidArgument if puzzle.reduce(:+) != (fsize) * (fsize + 1) / 2 
  end
  # Parses npuzzle in files
  def self.parse_file file
    data = file.readlines.map(&:chomp).filter {|line| line[0] != '#' and line != ""}
    data = data.join(" ").split(" ")
    size = data.slice!(0).to_i
    sanitycheck size,data 
    Puzzle.new(size, data.map(&:split).flatten.map(&:to_i))
  end
end
