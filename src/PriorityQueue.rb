# See wikipedia
class PriorityQueue
  attr_reader :queue
  def initialize 
    @queue = [] 
    @content = Hash.new(false)
  end

  # inserts an element into the correct position in the queue, using a rather naive implementation (O(n))
  def << elem, priority
    index = @queue.bsearch_index {|x| x[:priority] < priority}
    index = -1 if index == nil
    @queue.insert(index, {:data => elem, :priority => priority})
    @content[elem] = true
  end

  def include? element
    @content[element]
  end
  
  # pops the element with the smallest priority
  def pop
    data = nil
    data = @queue.pop[:data] if not @queue.empty? 
    @content[data] = false
    data
  end

  # returns the length of the queue
  def size
    @queue.size
  end
  alias insert <<
  alias length size
end
